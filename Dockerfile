FROM python:3.7-slim

#Implementation of Layer Caching
COPY requirements.txt /requirements.txt
RUN pip install -r /requirements.txt
COPY . /app
WORKDIR /app

ENTRYPOINT [ "/app/entrypoint.sh" ]
