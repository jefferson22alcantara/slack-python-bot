"""Top-level package for  SRE Slack bot."""

__progname__ = "pse-bot"
__version__ = "0.0.1"
__author__ = "jefferson"
__author_email__ = "jefferson22alcantara@gmail.com"
__license__ = "Proprietary"
__copyright__ = "Copyright (c) 2019 Jefferson S/A"
