import pse_bot
import re
import os
import slack
from slack.web.client import WebClient
from pse_bot.intent_classification.intent_classify import IntentClassify
from pse_bot.intents.jira_intent import Jira


#The WebSocket implementation intepreter this decorator to init the websocket server
@slack.RTMClient.run_on(event='message')
def handle_message(**payload):
    #All information that comes from slack message
    data = payload.get('data', '')
    web_client = payload.get('web_client', '')
    rtm_client = payload.get('rtm_client', '')

    intent_classify = None
    response = None

    try:
        intent_classify = IntentClassify(data)
        response = intent_classify.handle_command()
    except:
        pass

    if (response):

        client = slack.WebClient(token=os.environ['SLACK_API_TOKEN'])
        channel_id = data['channel']
        thread_ts = data['ts']
        
        try:
            print("####################################################")
            print("Execute PermaLink...")
            print("####################################################")

            thread_link = client.api_call(
                    api_method='chat.getPermalink',
                    params={'channel': channel_id,'message_ts': thread_ts}
                )
            print("Values from: ", thread_link)
        except:
            print("It was not possible to get the permalink from Slack thread.")
            pass

        web_client.chat_postMessage(
            channel=channel_id,
            text=f"{response}",
            thread_ts=thread_ts
        )
        match_re = os.environ.get("PROJECTKEY", "") + "-[0-9]+"
        issue = re.search(match_re, response)
        emptySelfinit = ''
        Jira(emptySelfinit).jira_comment_ticket(issue=issue.group(),thread_json=thread_link)




