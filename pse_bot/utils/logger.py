import logging

def do_log(log_type, message):
    '''
        Log levels: error, warning, info, debug
    '''
    if "error" in log_type:
        logging.error(message)
    elif "warning" in log_type:
        logging.warning(message)
    elif "info" in log_type:
        logging.info(message)
    elif "debug" in log_type:
        logging.debug(message)
        
class Logger(object):

    def __init__(self, log_config_level=None):
        logging.basicConfig(level=log_config_level)
        logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    


