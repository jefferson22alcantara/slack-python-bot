import slack
import os

def get_user_info(user_id):
    client = slack.WebClient(token=os.environ.get("SLACK_API_TOKEN", ""))
    user_info = client.users_info(user=user_id)
    return user_info