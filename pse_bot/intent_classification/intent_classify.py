import os

from pse_bot.intents.jira_intent import Jira
from pse_bot.intents.utils_intent import UltisIntent
from pse_bot.utils.logger import do_log
from pse_bot.intent_classification.commands import command_list

class IntentClassify(object):
    
    def __init__(self, message_info):
        self.message_info = message_info
        #Commands to intents
        self.command_list = command_list
    
    def handle_command(self):

        message = self.message_info.get("text")
        bot_info = self.message_info.get("bot_profile")

        if not message:
            return None

        #Validate if is a response for workflow (Response for a BOT)
        if "bot_profile" in self.message_info.keys():
            if os.environ.get("WORKFLOW_BOT_NAME", "") in bot_info.get("name"):
                #Intent slack_workflow
                do_log("info", "Creating Jira issue")
                try:
                    response = Jira(self.message_info)()
                    return response
                except Exception as e:
                    do_log("error", f"{e}")
                    return f"Error to create Issue {e}"

        #Validate if it's the APP_ID who sends the message
        if os.environ.get("APP_ID", "") in bot_info.get("app_id"):
            return None
        
        do_log("info",f"message: {self.message_info}")
        
        #Slack command sent to bot
        command_index = 1
        user_command = message.split(" ")

        if not len(user_command) > 1:

            return None
        
        user_command = message.split(" ")[command_index]

        if user_command == "help":
            message = ""
            do_log("info",f"message: help command")
            try:
                for key, value in self.command_list.items():
                    if not value.get("hidden"):
                        message += f"Command: {key} - {value.get('description')} \n"
                return message
            except Exception as e:
                return str(e)
        
        for command, key in self.command_list.items():
            if user_command in command or user_command in key.get("alias"):
                if not key.get("method"):
                    #Respond the command message
                    return key.get("message")

                print(f"[INFO] Going to execute the function {message}")
                response = key.get("method")(self.message_info)()
                print(response)
                return response
        
        return None