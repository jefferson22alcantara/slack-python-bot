from ifood_pse_bot.intents.utils_intent import UltisIntent

command_list =  {
    "kb": {
     "alias" : ["docs"],
     "method": UltisIntent,
     "description": "Get our knowledge base link",
     "message": "",
     "hidden": False
   },
   "version": {
     "alias" : [],
     "method": False,
     "description": "Bot version",
     "message": "v 1.0.2",
     "hidden": False
   }
 }