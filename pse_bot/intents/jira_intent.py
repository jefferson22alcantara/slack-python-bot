from pse_bot.utils.logger import do_log
from pse_bot.utils.slack_utils import get_user_info
from datetime import datetime

from jira import JIRA
import re
import os
import json

class Jira(object):
    
    def __init__(self, command_data):
        self.user = os.environ.get("JIRA_USER", "")
        self.api_token = os.environ.get("JIRA_API_TOKEN", "")
        self.jira_url = os.environ.get("JIRA_API_URL", "")
        self.command_data = command_data

    def __call__(self):
        return self.create_jira_issue()

    def create_jira_issue(self):
        jira = JIRA(self.jira_url, 
            basic_auth=(self.user, self.api_token))

        parsed_workflow_response = self.jira_message_parser()
        requester_id = parsed_workflow_response.get('Requester').strip()
        requester_id = ((requester_id.replace("<","")).replace(">","")).replace("@","")
        do_log("info", f"REQUESTER ID: {requester_id}")

        requester_info = get_user_info(requester_id)


        do_log("info", requester_info)
        tribe_name = str(parsed_workflow_response.get('Tribe')).strip()
        now = datetime.today().isoformat()

        description = f"*Requester*: {requester_info.get('user').get('name')} \n"+ \
            f"{parsed_workflow_response.get('Description')} \n"+ \
            f"*Tool*: {parsed_workflow_response.get('Tool')} \n"+ \
            f"*Env*: {parsed_workflow_response.get('Env')} \n"+ \
            f"*Tribe*: {tribe_name} \n"+ \
            f"*Date*: {now}"

        priority = str(parsed_workflow_response.get('Priority')).strip()
        summary = f"[{priority}]{str(parsed_workflow_response.get('Summary'))}"
        orginalEstimate = "2h" if priority == "Disaster" else "0m"

        
        issue_dict = {
            'project': {'key': os.environ.get("PROJECTKEY", "")},
            'summary': summary,
            'description': description,
            'priority' : { 'name' : priority },
            'issuetype': {'name': 'Task'}
            # "timetracking": {
            # 'originalEstimate': orginalEstimate
            # }
            # 'customfield_13630': {'value': tribe_name}
        }
        
        new_issue = jira.create_issue(issue_dict)
        return f"Here is your ticket: {self.jira_url}/browse/{new_issue} !"
    
    def jira_message_parser(self):
        parsed_response = {}
        arguments_list = self.command_data.get("text").split("\n")

        key_index = 0
        value_index = 1
        list_count = 0

        do_log("info", self.command_data)

        for argument in arguments_list:    
            key_value_list = argument.split(":")
            key = (key_value_list[key_index]).replace("*", "")
            #TODO: Remove mocked description to a structure that fills everything
            if key == "Description":
                value = arguments_list[6:]
                new_value = list(map(lambda v: str(v).strip(),value))
                string_value = ((" ".join(new_value)).replace("Description:","")).strip()
                print(string_value)
                parsed_response[key] = string_value
                break
            
            if key == "Priority":
                value = key_value_list[3]
            else:
                value = key_value_list[value_index]
            parsed_response[key] = value
            list_count+=1
        
        return parsed_response
    
    def jira_comment_ticket(self, issue, thread_json):
        jira = JIRA(self.jira_url, 
            basic_auth=(self.user, self.api_token))
        
        comments = thread_json
        new_comment = jira.add_comment(issue, comments['permalink'])
        print("Thread ", comments['permalink'] ," comment posted to ticket", issue)

        
