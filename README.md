#  Bot PSE

This Bot needs some Environment Variables: 

- WORKFLOW_BOT_NAME: PSE Support
- APP_ID: <@APP_ID>
- JIRA_USER: jira_user@jira.com
- JIRA_API_URL: https://rwondemand.atlassian.net
- PROJECTKEY: PSESUP
- SLACK_API_TOKEN: 
- JIRA_API_TOKEN: 

This BOT listen to messages in #channel where he was added. When WORKFLOW_BOT_NAME send message to the #channel, this BOT answers with a Jira Ticket.


## Install 
```
pip install -r requirement 
```

## Slack Classic app( IS Necessary create Slack Classic App)
https://api.slack.com/legacy/custom-integrations/legacy-tokens <p>
https://api.slack.com/apps?new_classic_app=1

# Export environents 
 ```
export WORKFLOW_BOT_NAME="PSE Support" 
export APP_ID="<@APP_ID>"
export JIRA_USER="jira_user@jira.com"
export JIRA_API_URL="https://rwondemand.atlassian.net"
export PROJECTKEY="PSESUP"
export SLACK_API_TOKEN="SLACK_TOKEN"
export JIRA_API_TOKEN="YOUR_JIRA_TOKEN"

```
# Run
``` 
python pse-bot.py 

```

