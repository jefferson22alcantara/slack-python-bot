#!/usr/bin/env python
#
# Convenience wrapper to the entrypoint script.
#
import logging
import multiprocessing
import os
import slack
import sys

from pse_bot.bot import handle_message
from pse_bot.utils.logger import Logger

def start_websocket():
    slack_token = os.environ.get("SLACK_API_TOKEN", "")
    rtm_client = slack.RTMClient(token=slack_token)
    rtm_client.start()

if __name__ == "__main__":

    if len(sys.argv) > 1 and sys.argv[1] == "status":
        print("Health !")
        exit(0)

    log = Logger(logging.INFO)
    start_websocket()
